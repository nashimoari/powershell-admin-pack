Param([string]$filename)

$checks = Import-CSV "$PSScriptRoot\..\$filename"

foreach ($check in $checks) {
  echo "---------------------------------------------------"
  echo $check.from
  if (Test-Connection -ComputerName $check.from -Quiet) {
    Invoke-Command -ComputerName $check.from -FilePath ps\check_connect.ps1 -Args $check.to, $check.port
  } else {
    echo "Нет доступа к серверу для удаленного запуска PowerShell скрипта"
  }
}